import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface categorySlice {
  categoryId: string;
  categoryName: string;
  playlistsPerCategory?: SpotifyApi.CategoryPlaylistsResponse;
}

const initialState: categorySlice = {
  categoryId: '0JQ5DAqbMKFEC4WFtoNRpw',
  categoryName: '',
};

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    addCategoryId(state, action: PayloadAction<string>) {
      state.categoryId = action.payload;
    },

    addplaylistsPerCategory(
      state,
      action: PayloadAction<SpotifyApi.CategoryPlaylistsResponse>
    ) {
      state.playlistsPerCategory = action.payload;
    },
    addCategoryName(state, action: PayloadAction<string>) {
      state.categoryName = action.payload;
    },
  },
});

export const { addCategoryId, addplaylistsPerCategory, addCategoryName } =
  categorySlice.actions;

export const selectCategoryValue = (state: RootState) => state.category;
export default categorySlice.reducer;
