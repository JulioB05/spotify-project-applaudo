import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface userSlice {
  userData?: SpotifyApi.CurrentUsersProfileResponse;
  userAccountLevel: string;
  userTracksSaved?: SpotifyApi.SavedTrackObject[];
}

const initialState: userSlice = {
  userAccountLevel: '',
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUserData(
      state,
      action: PayloadAction<SpotifyApi.CurrentUsersProfileResponse>
    ) {
      state.userData = action.payload;
    },

    addUserAccountLevel(state, action: PayloadAction<string>) {
      state.userAccountLevel = action.payload;
    },
    addUserTracksSaved(
      state,
      action: PayloadAction<SpotifyApi.SavedTrackObject[]>
    ) {
      state.userTracksSaved = action.payload;
    },
  },
});

export const { addUserData, addUserAccountLevel, addUserTracksSaved } =
  userSlice.actions;

export const selectUserValue = (state: RootState) => state.user;
export default userSlice.reducer;
