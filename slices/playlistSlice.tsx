import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface playlistState {
  playlistId: string;
  selectedPlaylist?: SpotifyApi.SinglePlaylistResponse;
  userPlaylists?: SpotifyApi.ListOfUsersPlaylistsResponse;
}

const initialState: playlistState = {
  playlistId: '37i9dQZF1E8FPpbkvZU3n9',
};

export const playlistSlice = createSlice({
  name: 'playlist',
  initialState,
  reducers: {
    addPlaylistById(state, action: PayloadAction<string>) {
      state.playlistId = action.payload;
    },

    addSelectedPlaylist(
      state,
      action: PayloadAction<SpotifyApi.SinglePlaylistResponse>
    ) {
      state.selectedPlaylist = action.payload;
    },
    addUserPlaylists(
      state,
      action: PayloadAction<SpotifyApi.ListOfUsersPlaylistsResponse>
    ) {
      state.userPlaylists = action.payload;
    },
  },
});

export const { addPlaylistById, addSelectedPlaylist, addUserPlaylists } =
  playlistSlice.actions;

export const selectPlaylistValue = (state: RootState) => state.playlist;
export default playlistSlice.reducer;
