import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface playlistState {
  albumId: string;
  selectedAlbum?: SpotifyApi.SingleAlbumResponse;
}

const initialState: playlistState = {
  albumId: '3bTNxJYk2bwdWBMtrjBxb0',
};

export const albumSlice = createSlice({
  name: 'album',
  initialState,
  reducers: {
    addAlbumId(state, action: PayloadAction<string>) {
      state.albumId = action.payload;
    },

    addSelectedAlbum(
      state,
      action: PayloadAction<SpotifyApi.SingleAlbumResponse>
    ) {
      state.selectedAlbum = action.payload;
    },
  },
});

export const { addAlbumId, addSelectedAlbum } = albumSlice.actions;

export const selectAlbumValue = (state: RootState) => state.album;
export default albumSlice.reducer;
