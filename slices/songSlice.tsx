import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface songSlice {
  currentTrackIdState: string | undefined;
  isPlayingState: boolean;
  songInfo?: SpotifyApi.SingleTrackResponse;
}

const initialState: songSlice = {
  currentTrackIdState: '',
  isPlayingState: false,
};

export const songSlice = createSlice({
  name: 'song',
  initialState,
  reducers: {
    addCurrentTrackId(state, action: PayloadAction<string | undefined>) {
      state.currentTrackIdState = action.payload;
    },

    addIsPlaying(state, action: PayloadAction<boolean>) {
      state.isPlayingState = action.payload;
    },
    addSongInfo(state, action: PayloadAction<SpotifyApi.SingleTrackResponse>) {
      state.songInfo = action.payload;
    },
  },
});

export const { addCurrentTrackId, addIsPlaying, addSongInfo } =
  songSlice.actions;

export const selectSongValue = (state: RootState) => state.song;
export default songSlice.reducer;
