import type { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../store';

export interface artistSlice {
  artistId: string | undefined;
  artistInfo?: SpotifyApi.SingleArtistResponse;
  artistAlbums?: SpotifyApi.ArtistsAlbumsResponse;
  artistTopTracks?: SpotifyApi.ArtistsTopTracksResponse;
  artistRelatedArtists?: SpotifyApi.ArtistsRelatedArtistsResponse;
}

const initialState: artistSlice = {
  artistId: '',
};

export const artistSlice = createSlice({
  name: 'artist',
  initialState,
  reducers: {
    addArtistId(state, action: PayloadAction<string | undefined>) {
      state.artistId = action.payload;
    },
    addArtistInfo(
      state,
      action: PayloadAction<SpotifyApi.SingleArtistResponse>
    ) {
      state.artistInfo = action.payload;
    },
    addArtistAlbums(
      state,
      action: PayloadAction<SpotifyApi.ArtistsAlbumsResponse>
    ) {
      state.artistAlbums = action.payload;
    },
    addArtistTopTracks(
      state,
      action: PayloadAction<SpotifyApi.ArtistsTopTracksResponse>
    ) {
      state.artistTopTracks = action.payload;
    },
    addArtistRelatedArtists(
      state,
      action: PayloadAction<SpotifyApi.ArtistsRelatedArtistsResponse>
    ) {
      state.artistRelatedArtists = action.payload;
    },
  },
});

export const {
  addArtistId,
  addArtistInfo,
  addArtistAlbums,
  addArtistTopTracks,
  addArtistRelatedArtists,
} = artistSlice.actions;

export const selectArtistValue = (state: RootState) => state.artist;
export default artistSlice.reducer;
