import {
  combineReducers,
  configureStore
} from '@reduxjs/toolkit';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import albumReducer from './slices/albumSlice';
import artistReducer from './slices/artistSlice';
import categoryReducer from './slices/categorySlice';
import playlistReducer from './slices/playlistSlice';
import songReducer from './slices/songSlice';
import userReducer from './slices/userSlice';

const persistConfiguration = {
  key: 'root',
  version:1,
  storage,
};

const reducer = combineReducers({
  playlist: playlistReducer,
  song: songReducer,
  user:userReducer,
  artist:artistReducer,
  album:albumReducer,
  category:categoryReducer
})


const persistReducerApp = persistReducer(persistConfiguration, reducer);
export const store = configureStore({
    reducer:persistReducerApp,
    middleware:(getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false})
  });

export const persistor = persistStore(store);

// export const store = configureStore({
//   reducer: {
// playlist: playlistReducer,
// song: songReducer
//   },
// });

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
