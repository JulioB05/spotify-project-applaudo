This is an app that uses the spotify api to recreate the original app.
It has implemented a deploy in vercel:
https://spotify-project-applaudo.vercel.app/login

In this app you can search by artist, song or playlist, it also has the following pages:

- Home page
  -Search
- My library
  -User
- Tracks
- Albums
- Playlist
- Login

You can also follow artists, songs or albums
