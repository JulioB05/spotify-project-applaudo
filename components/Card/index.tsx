import Link from 'next/link';
import { useDispatch } from 'react-redux';
import useSpotify from '../../hooks/useSpotify';
import { addArtistId } from '../../slices/artistSlice';
import { addPlaylistById } from '../../slices/playlistSlice';
import { addCurrentTrackId } from '../../slices/songSlice';

interface CardProps {
  image: string;
  title: string;
  artist?: string;
  popularity?: number;
  description?: string | null;
  id: string;
  type: string;
}

const Card = ({
  image,
  title,
  popularity,
  artist,
  description,
  id,
  type,
}: CardProps) => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();

  const addItemId = () => {
    if (type === 'track') {
      dispatch(addCurrentTrackId(id));
    } else if (type === 'playlist') {
      dispatch(addPlaylistById(id));
    } else if (type === 'artist') {
      dispatch(addArtistId(id));
    }
  };
  return (
    <>
      <Link href={`/${type}/${id}}`}>
        <div className="p-3 " onClick={addItemId}>
          <div className=" max-h-[450px] max-w-xs bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-900 dark:border-gray-700">
            <a href="#">
              <img className="rounded-t-lg h-80 w-80" src={image} alt="" />
            </a>
            <div className="p-5 h-40">
              <a href="#">
                <h5 className="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                  {title}
                </h5>
              </a>
              {popularity && (
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Popularity: {popularity}
                </p>
              )}

              {artist && (
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Artist: {artist}
                </p>
              )}
              {description && (
                <>
                  <p className="mb-2 font-normal text-gray-700 dark:text-gray-400 text-[12px] ">
                    Description: {description}
                  </p>
                </>
              )}
            </div>
          </div>
        </div>
      </Link>
    </>
  );
};

export default Card;
