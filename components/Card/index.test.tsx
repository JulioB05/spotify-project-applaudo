import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import Card from '../Card';

describe('Card', () => {
  it('renders a Card', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <Card
            image={
              'https://og-image.vercel.app/Testing%20with%20**Next.js**.png?theme=dark&md=1&fontSize=100px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-white-logo.svg'
            }
            title={'test'}
            id={'test'}
            type={'album'}
          />
        </Provider>
      </SessionProvider>
    );
  });
});
