import { faHeart } from '@fortawesome/free-regular-svg-icons';
import {
  faArrowRightFromBracket,
  faBook,
  faCirclePlus,
  faHouse,
  faMagnifyingGlass,
  faSquareRss,
} from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { signOut, useSession } from 'next-auth/react';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import useSpotify from '../../hooks/useSpotify';
import { addPlaylistById, addUserPlaylists } from '../../slices/playlistSlice';

const Sidebar = () => {
  const spotifyApi = useSpotify();
  const { data: session } = useSession();
  const [playlist, setPlaylist] =
    useState<SpotifyApi.PlaylistObjectSimplified[]>();

  const dispatch = useDispatch();

  useEffect(() => {
    if (spotifyApi.getAccessToken()) {
      spotifyApi.getUserPlaylists().then((data) => {
        setPlaylist(data.body.items);
        dispatch(addUserPlaylists(data.body));
      });
    }
  }, [session, spotifyApi]);

  return (
    <div>
      <div className="text-gray-500 p-5 text-xs lg:text-sm border-r border-gray-900 overflow-y-scroll scrollbar-hide h-screen sm:max-w-[12rem] lg:max-w-[15rem] hidden md:inline-flex pb-36">
        <div className="space-y-4">
          <button
            className="flex items-center space-x-2 hover:text-white"
            onClick={() => signOut()}
          >
            <FontAwesomeIcon
              icon={faArrowRightFromBracket}
              className="h-5 w-5"
            />
            <p>Log out</p>
          </button>
          <Link href={'/'}>
            <button className="flex items-center space-x-2 hover:text-white">
              <FontAwesomeIcon icon={faHouse} className="h-5 w-5" />
              <p>Home</p>
            </button>
          </Link>
          <Link href={'/search'}>
            <button className="flex items-center space-x-2 hover:text-white">
              <FontAwesomeIcon icon={faMagnifyingGlass} className="h-5 w-5" />
              <p>Search</p>
            </button>
          </Link>
          <Link href={'/library'}>
            <button className="flex items-center space-x-2 hover:text-white">
              <FontAwesomeIcon icon={faBook} className="h-5 w-5" />
              <p>Your Library</p>
            </button>
          </Link>

          <hr className="border-t-[0.1px] border-gray-900" />

          <button className="flex items-center space-x-2 hover:text-white">
            <FontAwesomeIcon icon={faCirclePlus} className="h-5 w-5" />
            <p>Create Playlist</p>
          </button>
          <button className="flex items-center space-x-2 hover:text-white">
            <FontAwesomeIcon icon={faHeart} className="h-5 w-5" />
            <p>Liked songs</p>
          </button>
          <button className="flex items-center space-x-2 hover:text-white">
            <FontAwesomeIcon icon={faSquareRss} className="h-5 w-5" />
            <p>Your episodes</p>
          </button>
          <hr className="border-t-[0.1px] border-gray-900" />

          {playlist?.map((itemPlaylist) => {
            const { name, id } = itemPlaylist;
            return (
              <Link href={`/playlist/${id}`} key={id}>
                <p
                  onClick={() => dispatch(addPlaylistById(id))}
                  className="cursor-pointer hover:text-white"
                >
                  {name}
                </p>
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
