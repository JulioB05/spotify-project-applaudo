import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import Sidebar from '../Sidebar';

describe('Sidebar', () => {
  it('renders a Sidebar', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <Sidebar />
        </Provider>
      </SessionProvider>
    );
  });
});
