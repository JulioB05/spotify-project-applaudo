import { faCirclePlay } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { addAlbumId } from '../../slices/albumSlice';

interface PosterProps {
  data: SpotifyApi.AlbumObjectSimplified;
}

const Poster = ({ data }: PosterProps) => {
  const dispatch = useDispatch();
  const { id, images, artists } = data;
  const artistName = artists?.[0]?.name;

  const img = images?.[0]?.url
    ? images?.[0]?.url
    : 'https://t2.tudocdn.net/600663?w=1920';

  const addTrackId = () => {
    dispatch(addAlbumId(id));
  };
  return (
    <Link href={`/album/${id}`}>
      <div
        onClick={addTrackId}
        className="w-[260px] h-[360px] rounded-[50px] overflow-hidden relative text-white/80 cursor-pointer hover:scale-105 hover:text-white/100 transition duration-200 ease-out group mx-auto"
      >
        <img
          src={img}
          alt=""
          className="h-full w-full absolute inset-0 object-cover rounded-[50px] opacity-80 group-hover:opacity-100"
        />

        <div className="absolute bottom-10 inset-x-0 ml-4 flex items-center space-x-3.5">
          <div className="h-10 w-10 bg-gray-900 rounded-full flex items-center justify-center group-hover:bg-[#1db954] flex-shrink-0">
            <FontAwesomeIcon icon={faCirclePlay} className={'h-5/6'} />
          </div>

          <div className="text-[15px]">
            <h4 className="font-extrabold truncate w-44">{data.name}</h4>
            <h6>{artistName}</h6>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default Poster;
