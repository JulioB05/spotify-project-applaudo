import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import Modal from '../Modal';

const setActiveModalMock = jest.fn;
describe('Modal', () => {
  it('renders a Modal', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <Modal
            title={''}
            id={''}
            type={''}
            follow={false}
            setActiveModal={setActiveModalMock}
          />
        </Provider>
      </SessionProvider>
    );
  });
});
