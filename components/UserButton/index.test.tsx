import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import UserButton from '../UserButton';

describe('UserButton', () => {
  it('renders a UserButton', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <UserButton />
        </Provider>
      </SessionProvider>
    );
  });
});
