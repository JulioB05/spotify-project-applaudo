import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSession } from 'next-auth/react';
import Link from 'next/link';

const UserButton = () => {
  const { data: session } = useSession();
  const { image, name } = session?.user!;
  return (
    <div className="z-10">
      <Link href={'/user'}>
        <header className="absolute top-5 right-8 text-white">
          <div className="flex items-center bg-gray-800 space-x-3 opacity-90 hover:opacity-80 cursor-pointer rounded-full p-1 pr-2 ">
            <img
              className="rounded-full w-10 h-10"
              src={
                image
                  ? image
                  : 'https://thumbs.dreamstime.com/b/icono-de-usuario-predeterminado-vectores-imagen-perfil-avatar-predeterminada-vectorial-medios-sociales-retrato-182347582.jpg'
              }
              alt="user-image"
            />
            <h2>{name}</h2>
            <FontAwesomeIcon icon={faChevronDown} />
          </div>
        </header>
      </Link>
    </div>
  );
};

export default UserButton;
