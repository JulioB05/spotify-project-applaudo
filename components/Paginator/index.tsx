import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Dispatch, SetStateAction, useEffect, useMemo } from 'react';
import ReactPaginate from 'react-paginate';

interface PaginatorProps {
  currentPage: number;
  sizeOfData: number;
  setCurrentPage: Dispatch<SetStateAction<number>>;
}

const Paginator = ({
  currentPage,
  sizeOfData,
  setCurrentPage,
}: PaginatorProps) => {
  const maxNumberPage = useMemo(() => {
    if (sizeOfData > 1000) {
      return Math.ceil(950 / 22);
    }
    return Math.ceil(sizeOfData / 22);
  }, [sizeOfData]);

  const handlePageClick = (event: { selected: number }) => {
    setCurrentPage(event.selected + 1);
  };

  useEffect(() => {}, [currentPage]);
  return (
    <div className="w-full bg-center-color-gray h-10 flex items-center ">
      <ReactPaginate
        forcePage={currentPage - 1}
        pageCount={maxNumberPage ? maxNumberPage : 1}
        nextLabel={<FontAwesomeIcon icon={faAngleRight} />}
        previousLabel={<FontAwesomeIcon icon={faAngleLeft} />}
        className="flex flex-row w-5/6 justify-evenly items-center"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        activeClassName="text-blue-800 text-[25px] font-bold"
      />
    </div>
  );
};

export default Paginator;
