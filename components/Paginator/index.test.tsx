import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import Paginator from '../Paginator';

const setCurrentPageMock = jest.fn;

describe('Paginator', () => {
  it('renders a Paginator', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <Paginator
            currentPage={0}
            sizeOfData={0}
            setCurrentPage={setCurrentPageMock}
          />
        </Provider>
      </SessionProvider>
    );
  });
});
