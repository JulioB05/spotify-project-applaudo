import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import FollowComponent from '../FollowComponent';

describe('FollowComponent', () => {
  it('renders a FollowComponent', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <FollowComponent id={'54asda'} type={'artist'} title={'test'} />
        </Provider>
      </SessionProvider>
    );
  });
});
