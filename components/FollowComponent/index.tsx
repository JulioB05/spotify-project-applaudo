import { faHeart } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import useSpotify from '../../hooks/useSpotify';
import Modal from '../Modal';

interface FollowComponentProps {
  id: string;
  type: string;
  title: string;
}

const FollowComponent = ({ id, type, title }: FollowComponentProps) => {
  const [activeModal, setActiveModal] = useState(false);
  const [follow, setfollow] = useState(false);
  const spotifyApi = useSpotify();

  const checkFollowArtist = () => {
    (async () => {
      spotifyApi.isFollowingArtists([id]).then(
        function (data) {
          let isFollowing = data.body;
          setfollow(isFollowing[0]);
        },
        function (err) {
          console.error('Something went wrong!', err);
        }
      );
    })();
  };
  const checkFollowTrack = () => {
    (async () => {
      spotifyApi.containsMySavedTracks([id]).then(
        function (data) {
          // An array is returned, where the first element corresponds to the first track ID in the query
          var trackIsInYourMusic = data.body[0];
          setfollow(trackIsInYourMusic);
        },
        function (err) {
          console.error('Something went wrong!', err);
        }
      );
    })();
  };
  const checkFollowAlbum = () => {
    (async () => {
      spotifyApi.containsMySavedAlbums([id]).then(
        function (data) {
          // An array is returned, where the first element corresponds to the first album ID in the query
          var albumIsInYourMusic = data.body[0];
          setfollow(albumIsInYourMusic);
        },
        function (err) {
          console.error('Something went wrong!', err);
        }
      );
    })();
  };

  const addFollowUnfollow = () => {
    setActiveModal(true);
    switch (type) {
      case 'artist': {
        checkFollowArtist();
        break;
      }
      case 'album': {
        checkFollowAlbum();
        break;
      }
      case 'track': {
        checkFollowTrack();
      }
      default:
        break;
    }
  };

  return (
    <div>
      <div className="cursor-pointer pl-5" onClick={addFollowUnfollow}>
        <FontAwesomeIcon icon={faHeart} className="h-7 w-7" />
      </div>
      {activeModal && (
        <Modal
          title={title}
          id={id}
          type={type}
          follow={follow}
          setActiveModal={setActiveModal}
        />
      )}
    </div>
  );
};

export default FollowComponent;
