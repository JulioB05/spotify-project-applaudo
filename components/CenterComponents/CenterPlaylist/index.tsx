import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';
import { addSelectedAlbum, selectAlbumValue } from '../../../slices/albumSlice';

import {
  addSelectedPlaylist,
  selectPlaylistValue,
} from '../../../slices/playlistSlice';
import FollowComponent from '../../FollowComponent';
import Songs from '../../Songs';
import UserButton from '../../UserButton';

interface CenterPlaylistProps {
  type: string;
}

const CenterPlaylist = ({ type }: CenterPlaylistProps) => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();

  const { playlistId, selectedPlaylist } = useSelector(selectPlaylistValue);
  const { albumId, selectedAlbum } = useSelector(selectAlbumValue);

  useEffect(() => {
    if (spotifyApi.getAccessToken() && type === 'playlist') {
      spotifyApi
        .getPlaylist(playlistId)
        .then((data) => {
          dispatch(addSelectedPlaylist(data.body));
        })
        .catch((err) => console.error('Something went wrong', err));
    } else if (spotifyApi.getAccessToken() && type === 'album') {
      spotifyApi
        .getAlbum(albumId)
        .then((data) => {
          dispatch(addSelectedAlbum(data.body));
        })
        .catch((err) => console.error('Something went wrong', err));
    }
  }, [playlistId]);

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
      <UserButton />
      <section
        className={`flex items-end space-x-7 bg-center-color-purple h-80 text-white p-8`}
      >
        <img
          src={
            type === 'playlist'
              ? selectedPlaylist?.images?.[0]?.url
              : selectedAlbum?.images?.[0]?.url
          }
          alt="image-song"
          className="h-44 w-44 shadow-2xl"
        />
        <div>
          {type === 'playlist' ? <p>PLAYLIST</p> : <p>ALBUM</p>}
          <div className="flex item-center">
            <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold">
              {type === 'playlist'
                ? selectedPlaylist?.name
                : selectedAlbum?.name}
            </h1>
            {type === 'playlist' ? (
              ''
            ) : (
              <FollowComponent
                id={selectedAlbum?.id!}
                type={'album'}
                title={selectedAlbum?.name!}
              />
            )}
          </div>
        </div>
      </section>
      <div>
        <Songs type={type} />
      </div>
    </div>
  );
};

export default CenterPlaylist;
