import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterPlaylist from '../CenterPlaylist';

describe('CenterPlaylist', () => {
  it('renders a CenterPlaylist', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterPlaylist type={'playlist'} />
        </Provider>
      </SessionProvider>
    );
  });
});
