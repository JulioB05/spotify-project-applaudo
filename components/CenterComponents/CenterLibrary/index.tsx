import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';
import { selectPlaylistValue } from '../../../slices/playlistSlice';
import { addUserTracksSaved, selectUserValue } from '../../../slices/userSlice';
import MiniCard from '../../MiniCard';
import Song from '../../Songs/song';

const CenterLibrary = () => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();

  const { userPlaylists } = useSelector(selectPlaylistValue);
  const { userData, userTracksSaved } = useSelector(selectUserValue);

  useEffect(() => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi.getMySavedTracks({ limit: 10 }).then(
          function (data) {
            dispatch(addUserTracksSaved(data.body.items));
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
      }
    })();
  }, [spotifyApi]);

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
      <section
        className={`flex items-center space-x-7 bg-center-color-indigo text-white p-8 h-2/4`}
      >
        <img
          src={
            userData?.images?.[0]?.url
              ? userData?.images?.[0]?.url
              : 'https://thumbs.dreamstime.com/b/icono-de-usuario-predeterminado-vectores-imagen-perfil-avatar-predeterminada-vectorial-medios-sociales-retrato-182347582.jpg'
          }
          alt={userData?.display_name}
          className="h-60 w-60 shadow-2xl"
        />
        <div>
          <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold pb-2">
            My library
          </h1>
        </div>
      </section>

      <section className="flex flex-col">
        {/* TOP ALBUMS */}

        <div>
          <div className="px-8">
            <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2 ">
              PLAYLISTS
            </h1>
          </div>
          <div className="px-8 grid grid-cols-2 pb-10 text-white">
            {userPlaylists?.items?.map((playlist, i) => {
              const { id, images, name } = playlist;
              const img = images?.[0]?.url
                ? images?.[0]?.url
                : 'https://t2.tudocdn.net/600663?w=1920';
              return (
                <MiniCard
                  key={id}
                  id={id}
                  title={name}
                  img={img}
                  type={'playlist'}
                  order={i}
                />
              );
            })}
          </div>
        </div>
        {/* TOP TRACKS */}
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            TOP 10 SAVED TRACKS
          </h1>
          {userTracksSaved?.map((trackItem, i) => {
            const { track } = trackItem;
            return (
              <Song key={track?.id} order={i} track={track} type={'playlist'} />
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default CenterLibrary;
