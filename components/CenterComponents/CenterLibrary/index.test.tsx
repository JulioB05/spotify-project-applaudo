import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterLibrary from '../CenterLibrary';

describe('CenterLibrary', () => {
  it('renders a CenterLibrary', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterLibrary />
        </Provider>
      </SessionProvider>
    );
  });
});
