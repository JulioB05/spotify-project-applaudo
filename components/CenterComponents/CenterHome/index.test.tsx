import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterHome from '../CenterHome';

describe('CenterHome', () => {
  it('renders a CenterHome', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterHome />
        </Provider>
      </SessionProvider>
    );
  });
});
