import Link from 'next/link';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';
import { addCategoryId, addCategoryName } from '../../../slices/categorySlice';
import AvatarPlaylist from '../../AvatarPlaylist';
import Poster from '../../Poster';

const CenterHome = () => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();
  const [categories, setCategories] = useState<SpotifyApi.CategoryObject[]>();
  const [newReleases, setNewReleases] =
    useState<SpotifyApi.AlbumObjectSimplified[]>();
  const [featurePlaylist, setFeaturePlaylist] =
    useState<SpotifyApi.PlaylistObjectSimplified[]>();

  useEffect(() => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi
          .getCategories({
            limit: 11,
            offset: 0,
          })
          .then(
            function (data) {
              setCategories(data.body.categories.items);
            },
            function (err) {
              console.error('Something went wrong!', err);
            }
          );

        spotifyApi.getNewReleases({ limit: 8, offset: 0, country: 'MX' }).then(
          function (data) {
            setNewReleases(data.body.albums.items);
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
        spotifyApi
          .getFeaturedPlaylists({
            limit: 4,
            offset: 1,
            country: 'MX',
          })
          .then(
            function (data) {
              setFeaturePlaylist(data.body.playlists.items);
            },
            function (err) {
              console.error('Something went wrong!', err);
            }
          );
      }
    })();
  }, [spotifyApi]);

  const addCategoryData = (id: string, name: string) => {
    dispatch(addCategoryId(id));
    dispatch(addCategoryName(name));
  };
  return (
    <section className={`  pb-4 space-y-6 md:max-w-auto flex-grow md:mr-2.5  `}>
      <div className={`bg-center-color-blue`}>
        <div>
          <h1
            className={`text-xl md:text-2xl xl:text-3xl font-bold pb-2 pl-5 `}
          >
            ALBUM RELEASES
          </h1>

          <div className="grid overflow-y-scroll scrollbar-hide h-96 py-4 grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-4 gap-y-8 ">
            {newReleases?.map((newRelease) => {
              return <Poster key={newRelease.id} data={newRelease} />;
            })}
          </div>
        </div>
      </div>
      <div className="flex gap-x-8 absolute min-w-full md:relative ml-6">
        {/* CATEGORIES */}
        <div className=" xl:inline max-w-[270px]">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            CATEGORIES
          </h1>
          <div className="flex gap-x-2 gap-y-2.5 flex-wrap mb-3">
            {categories?.map((category, i) => {
              return (
                <Link
                  key={category.id + i + 1}
                  href={`/category/${category.id}`}
                >
                  <div
                    onClick={() => addCategoryData(category.id, category.name)}
                    className="bg-[#101010] text-[#CECECE] border border-[#484848] rounded-xl py-2.5 px-3.5 text-[11px] font-bold  cursor-pointer"
                  >
                    {category.name}
                  </div>
                </Link>
              );
            })}
          </div>
        </div>

        {/* Tracks */}
        <div className="flex flex-col  ">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            FEATURE PLAYLIST
          </h1>
          {featurePlaylist?.map((item) => {
            const { images, name, description, id } = item;
            const img = images?.[0]?.url
              ? images?.[0].url
              : 'https://t2.tudocdn.net/600663?w=1920';

            return (
              <AvatarPlaylist
                key={id}
                image={img}
                title={name}
                description={description!}
                id={id}
              />
            );
          })}
        </div>
      </div>
    </section>
  );
};

export default CenterHome;
