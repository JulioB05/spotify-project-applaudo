import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';
import { selectCategoryValue } from '../../../slices/categorySlice';
import MiniCard from '../../MiniCard';

const CenterCategory = () => {
  const { categoryId, categoryName } = useSelector(selectCategoryValue);
  const spotifyApi = useSpotify();
  const [playlistsPerCategory, setPlaylistPerCategory] =
    useState<SpotifyApi.CategoryPlaylistsResponse>();

  useEffect(() => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi
          .getPlaylistsForCategory(categoryId, {
            country: 'MX',
            limit: 10,
            offset: 0,
          })
          .then(
            function (data) {
              setPlaylistPerCategory(data.body);
            },
            function (err) {
              console.error('Something went wrong!', err);
            }
          );
      }
    })();
  }, [categoryId]);

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
      <section
        className={`flex items-center space-x-7 bg-center-color-red  text-white p-8 h-2/4`}
      >
        <img
          src="http://www.tuconcierto.net/wp-content/uploads/2019/12/twitter_card-default.jpg"
          alt={categoryName}
          className="h-60 w-60 shadow-2xl"
        />
        <div>
          <p>ARTIST</p>
          <div className="flex items-center">
            <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold pb-2">
              Top 10 playlists for category "{categoryName}"
            </h1>
          </div>
        </div>
      </section>
      <section className="flex flex-col">
        {/* TOP ALBUMS */}
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
          {playlistsPerCategory?.playlists?.items?.map((playlist, i) => {
            const { id, name, images } = playlist;
            const img = images?.[0].url
              ? images?.[0].url
              : 'https://t2.tudocdn.net/600663?w=1920';
            return (
              <MiniCard
                key={id + i}
                id={id}
                title={name}
                img={img}
                type={'playlist'}
                order={i}
              />
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default CenterCategory;
