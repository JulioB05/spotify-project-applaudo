import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterSearch from '../CenterSearch';

describe('CenterSearch', () => {
  it('renders a CenterSearch', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterSearch />
        </Provider>
      </SessionProvider>
    );
  });
});
