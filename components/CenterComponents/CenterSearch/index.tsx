import { debounce } from 'lodash';
import { ChangeEvent, useEffect, useState } from 'react';
import useDataFounded from '../../../hooks/useDataFounded';
import Loader from '../../Loader';
import Paginator from '../../Paginator';
import SearchItems from '../../SearchItems';

interface CenterSearchProps {}

const CenterSearch = ({}: CenterSearchProps) => {
  const {
    getDataArtists,
    getDataPlaylists,
    getDataTracks,
    dataFounded,
    isLoading,
  } = useDataFounded();
  const [searchParams, setSearchParams] = useState({
    search: '',
    type: '',
  });
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    if (searchParams.search !== '' && searchParams.type !== '') {
      switch (searchParams.type) {
        case 'artist': {
          getDataArtists(currentPage, searchParams.search);

          break;
        }
        case 'track': {
          getDataTracks(currentPage, searchParams.search);

          break;
        }
        case 'playlist': {
          getDataPlaylists(currentPage, searchParams.search);

          break;
        }
        default:
          break;
      }
    }
  }, [searchParams, currentPage]);

  const debounceSearch = debounce((word: string) => {
    setSearchParams({ ...searchParams, search: word });
    setCurrentPage(1);
  }, 500);
  const searchFunction = (e: ChangeEvent<HTMLSelectElement>) => {
    setSearchParams({ ...searchParams, type: String(e.target.value) });
    setCurrentPage(1);
  };

  return (
    <div className="text-white  items-start justify-center w-full  h-screen overflow-y-scroll scrollbar-hide ">
      <div className="flex p-4 z-20  w-5/6 bg-black fixed">
        <div className="flex pl-2 pr-2">
          <select
            id="countries"
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            onChange={(e) => searchFunction(e)}
          >
            <option value="none">Choose a type</option>
            <option value="artist">Artist</option>
            <option value="track">Song</option>
            <option value="playlist">Playlist</option>
          </select>
        </div>

        <form className=" w-5/6">
          <label className="sr-only">Search</label>
          <div className="relative w-full">
            <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
              <svg
                aria-hidden="true"
                className="w-5 h-5 text-gray-500 dark:text-gray-400"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                  clipRule="evenodd"
                ></path>
              </svg>
            </div>
            <input
              type="text"
              id="simple-search"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Search"
              onChange={(e) => debounceSearch(String(e.target.value))}
              required
            />
          </div>
        </form>
      </div>
      <div className="container mx-auto mb-10 mt-16">
        {isLoading ? (
          <SearchItems items={dataFounded} type={searchParams.type} />
        ) : (
          <Loader />
        )}
      </div>
      <div className=" bottom-0 m-0 top-full fixed w-full flex text-white items-end ">
        <Paginator
          currentPage={currentPage}
          sizeOfData={dataFounded?.sizeOfData!}
          setCurrentPage={setCurrentPage}
        />
      </div>
    </div>
  );
};

export default CenterSearch;
