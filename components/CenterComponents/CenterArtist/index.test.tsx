import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterArtist from '../CenterArtist';

describe('CenterArtist', () => {
  it('renders a CenterArtist', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterArtist />
        </Provider>
      </SessionProvider>
    );
  });
});
