import { useSelector } from 'react-redux';
import { selectArtistValue } from '../../../slices/artistSlice';
import FollowComponent from '../../FollowComponent';
import MiniCard from '../../MiniCard';
import Song from '../../Songs/song';

const CenterArtist = () => {
  const { artistInfo, artistAlbums, artistTopTracks, artistRelatedArtists } =
    useSelector(selectArtistValue);

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
      <section
        className={`flex items-center space-x-7 bg-center-color-red  text-white p-8 h-2/4`}
      >
        <img
          src={artistInfo?.images?.[0]?.url}
          alt={artistInfo?.name}
          className="h-60 w-60 shadow-2xl"
        />
        <div>
          <p>ARTIST</p>
          <div className="flex items-center">
            <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold pb-2">
              {artistInfo?.name}
            </h1>
            <FollowComponent
              id={artistInfo?.id!}
              type={'artist'}
              title={artistInfo?.name!}
            />
          </div>
          <div className="flex items-center flex-wrap justify-evenly">
            Genres:
            {artistInfo?.genres.map((genre, i) => {
              return (
                <p className="pb-2 pt-2" key={i + 1}>
                  - {genre} -
                </p>
              );
            })}
          </div>

          <p className="pb-2 pt-2">Popularity: {artistInfo?.popularity}</p>
          <p className="pb-2 pt-2">Followers: {artistInfo?.followers?.total}</p>
        </div>
      </section>

      <section className="flex flex-col">
        {/* TOP TRACKS */}
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            TOP TRACKS
          </h1>
          {artistTopTracks?.tracks?.map((track, i) => {
            return (
              <Song key={track!.id} order={i} track={track} type={'playlist'} />
            );
          })}
        </div>
        {/* TOP ALBUMS */}
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            TOP ALBUMS
          </h1>
          {artistAlbums?.items?.map((albums, i) => {
            const { id, name, images } = albums;
            const img = images?.[0].url
              ? images?.[0].url
              : 'https://t2.tudocdn.net/600663?w=1920';
            return (
              <MiniCard
                key={id + i}
                id={id}
                title={name}
                img={img}
                type={'album'}
                order={i}
              />
            );
          })}
        </div>
        {/* FOLLOWED ARTISTS */}
        <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
          <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
            RELATED ARTISTS
          </h1>
          {artistRelatedArtists?.artists?.map((artist, i) => {
            const { id, name, images } = artist;
            const img = images?.[0].url
              ? images?.[0].url
              : 'https://t2.tudocdn.net/600663?w=1920';
            return (
              <MiniCard
                key={id}
                id={id}
                title={name}
                img={img}
                type={'artist'}
                order={i}
              />
            );
          })}
        </div>
      </section>
    </div>
  );
};

export default CenterArtist;
