import { useSelector } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';

import { selectSongValue } from '../../../slices/songSlice';
import { selectUserValue } from '../../../slices/userSlice';
import CustomPlayer from '../../CustomPlayer';
import FollowComponent from '../../FollowComponent';
import SpotifyPlayerWeb from '../../SpotifyPlayerWeb';
import UserButton from '../../UserButton';

const CenterSong = () => {
  const spotifyApi = useSpotify();

  const { songInfo } = useSelector(selectSongValue);
  const { userAccountLevel } = useSelector(selectUserValue);

  const artistName = songInfo?.artists?.[0]?.name;

  const img = songInfo?.album.images?.[0]?.url
    ? songInfo?.album.images?.[0]?.url
    : 'https://t2.tudocdn.net/600663?w=1920';

  return (
    <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
      <UserButton />

      <section
        className={`flex items-center space-x-7 bg-center-color-green h-5/6  text-white p-8`}
      >
        <img src={img} alt={songInfo?.name} className="h-60 w-60 shadow-2xl" />
        <div>
          <p>SONG</p>
          <div className="flex items-center">
            <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold">
              {songInfo?.name}
            </h1>
            <FollowComponent
              id={songInfo?.id!}
              type={'track'}
              title={songInfo?.name!}
            />
          </div>
          <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold">
            {artistName}
          </h1>
          <p>{songInfo?.album.name}</p>
        </div>
      </section>

      {userAccountLevel === 'premium' && spotifyApi.getAccessToken() ? (
        <SpotifyPlayerWeb />
      ) : (
        <CustomPlayer />
      )}
    </div>
  );
};

export default CenterSong;
