import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../../store';
import mockSession from '../../../testUtils/mockSession';
import CenterSong from '../CenterSong';

describe('CenterSong', () => {
  it('renders a CenterSong', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CenterSong />
        </Provider>
      </SessionProvider>
    );
  });
});
