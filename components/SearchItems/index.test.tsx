import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import SearchItems from '../SearchItems';

describe('SearchItems', () => {
  it('renders a SearchItems', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <SearchItems items={undefined} type={''} />
        </Provider>
      </SessionProvider>
    );
  });
});
