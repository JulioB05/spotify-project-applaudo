import { DataFoundedTypes } from '../../hooks/useDataFounded';
import Card from '../Card';

interface SearchItemsProps {
  items: DataFoundedTypes | undefined;
  type: string;
}

const SearchItems = ({ items, type }: SearchItemsProps) => {
  return (
    <div className="flex flex-wrap items-center justify-center">
      {type === 'artist' &&
        items?.artists?.items.map((artist, i) => {
          const { id, images, name, popularity } = artist;
          const img = images?.[0]?.url
            ? images?.[0].url
            : 'https://t2.tudocdn.net/600663?w=1920';

          return (
            <Card
              key={id}
              id={id}
              type={'artist'}
              image={img}
              title={name}
              popularity={popularity}
            />
          );
        })}
      {type === 'playlist' &&
        items?.playlits?.items.map((playlist, i) => {
          const { id, images, name, description } = playlist;
          const img = images?.[0]?.url
            ? images?.[0].url
            : 'https://t2.tudocdn.net/600663?w=1920';
          return (
            <Card
              key={id + i}
              image={img}
              title={name}
              description={description}
              id={id}
              type={'playlist'}
            />
          );
        })}
      {type === 'track' &&
        items?.tracks?.items.map((track, i) => {
          const { id, album, name, artists } = track;
          let img = album.images?.[0]?.url
            ? album.images?.[0]?.url
            : 'https://t2.tudocdn.net/600663?w=1920';
          let artistName = artists?.[0].name;
          return (
            <Card
              key={id}
              image={img}
              title={name}
              artist={artistName}
              id={id}
              type={'track'}
            />
          );
        })}
    </div>
  );
};

export default SearchItems;
