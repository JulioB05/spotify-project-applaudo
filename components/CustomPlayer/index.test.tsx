import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import CustomPlayer from '../CustomPlayer';

describe('CustomPlayer', () => {
  it('renders a CustomPlayer', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <CustomPlayer />
        </Provider>
      </SessionProvider>
    );
  });
});
