import {
  faCirclePause,
  faCirclePlay,
} from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import useSongInfo from '../../hooks/useSongInfo';
import useSpotify from '../../hooks/useSpotify';
import { selectSongValue } from '../../slices/songSlice';

const CustomPlayer = () => {
  const spotifyApi = useSpotify();
  const { songInfo } = useSelector(selectSongValue);
  const preview = songInfo?.preview_url;

  const [playSong, setPlaySong] = useState(false);
  const [songElement, setSongElement] = useState<HTMLAudioElement>();

  useSongInfo();

  const handlePlayPause = async () => {
    if (spotifyApi.getAccessToken()) {
      if (songElement?.paused) {
        playAudio();
      } else {
        songElement?.pause();
        setPlaySong(false);
      }
    }
  };

  async function playAudio() {
    try {
      await songElement?.play();
      setPlaySong(true);
    } catch (err) {
      console.error(err);
    }
  }
  useEffect(() => {
    if (preview) {
      const previousTrack = new Audio(preview);
      setSongElement(previousTrack);
    }
  }, [songInfo]);
  useEffect(() => {
    return songElement?.pause();
  }, []);
  return (
    <div>
      {/* left */}
      <div className="flex items-center space-x-4">
        <img
          className="hidden md:inline h-10 w-10"
          src={songInfo?.album.images?.[0]?.url}
          alt=""
        />
        <div>
          <h3>{songInfo?.name}</h3>
          <p>{songInfo?.artists?.[0].name}</p>
        </div>
      </div>
      {/* Center */}
      <div className="flex items-center justify-evenly ">
        {songInfo?.preview_url && playSong ? (
          <FontAwesomeIcon
            icon={faCirclePause}
            onClick={handlePlayPause}
            className="h-10 w-10 cursor-pointer hover:scale-125 transition transform duration-200 ease-out"
          />
        ) : (
          <FontAwesomeIcon
            icon={faCirclePlay}
            onClick={handlePlayPause}
            className="h-10 w-10 cursor-pointer hover:scale-125 transition transform duration-200 ease-out"
          />
        )}
      </div>
    </div>
  );
};

export default CustomPlayer;
