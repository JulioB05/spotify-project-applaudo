import Link from 'next/link';
import { useDispatch } from 'react-redux';
import useSpotify from '../../../hooks/useSpotify';
import timeConvertion from '../../../lib/time';
import { addCurrentTrackId } from '../../../slices/songSlice';

interface songProps {
  order: number;
  track?: SpotifyApi.TrackObjectFull;
  albumTrack?: SpotifyApi.TrackObjectSimplified;
  albumImage?: string;
  type: string;
}

const Song = ({ order, track, type, albumTrack, albumImage }: songProps) => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();

  const addTrackInfo = () => {
    if (type === 'playlist') {
      dispatch(addCurrentTrackId(track!.id));
    } else if (type === 'album') {
      dispatch(addCurrentTrackId(albumTrack!.id));
    }
  };

  return (
    <Link
      href={
        type === 'playlist' ? `/track/${track!.id}` : `/track/${albumTrack!.id}`
      }
    >
      <div
        className="grid grid-cols-2 text-gray-500 py-4 px-5 hover:bg-gray-900 rounderd-lg cursor-pointer"
        onClick={addTrackInfo}
      >
        <div className="flex items-center space-x-4">
          <p>{order + 1}</p>
          <img
            className="h-10 w-10"
            src={
              type === 'playlist' ? track!.album.images?.[0]?.url : albumImage
            }
            alt={type === 'playlist' ? track!.album.name : albumTrack?.name}
          />

          <div>
            <p className="w-36 lg:w-64 text-white truncate">
              {type === 'playlist' ? track!.album.name : albumTrack?.name}
            </p>
            <p className="w-40">
              {type === 'playlist'
                ? track!.artists?.[0]?.name
                : albumTrack?.artists?.[0]?.name}
            </p>
          </div>
        </div>

        <div className="flex items-center justify-between ml-auto md:ml-0">
          <p className="w-40 hidden md:inline">
            {type === 'playlist' ? track?.album?.name : albumTrack?.name}
          </p>
          <p>
            {timeConvertion(
              type === 'playlist' ? track!.duration_ms : albumTrack!.duration_ms
            )}
          </p>
        </div>
      </div>
    </Link>
  );
};

export default Song;
