import { useSelector } from 'react-redux';
import { selectAlbumValue } from '../../slices/albumSlice';
import { selectPlaylistValue } from '../../slices/playlistSlice';
import Song from './song';

interface SongsProps {
  type: string;
}

const Songs = ({ type }: SongsProps) => {
  const { selectedPlaylist } = useSelector(selectPlaylistValue);
  const { selectedAlbum } = useSelector(selectAlbumValue);

  return (
    <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
      {type === 'playlist'
        ? selectedPlaylist?.tracks.items.map((track, i) => {
            return (
              <Song
                key={track.track!.id + i + 1}
                order={i}
                track={track.track!}
                type={type}
              />
            );
          })
        : selectedAlbum?.tracks.items.map((track, i) => {
            return (
              <Song
                key={track!.id + i + 1}
                order={i}
                albumTrack={track}
                type={type}
                albumImage={selectedAlbum?.images?.[0]?.url}
              />
            );
          })}
    </div>
  );
};

export default Songs;
