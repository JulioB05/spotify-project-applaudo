import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import Songs from '../Songs';

describe('Songs', () => {
  it('renders a Songs', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <Songs type={'playlist'} />
        </Provider>
      </SessionProvider>
    );
  });
});
