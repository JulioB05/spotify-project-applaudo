import { useSelector } from 'react-redux';
import SpotifyPlayer from 'react-spotify-web-playback';
import useSongInfo from '../../hooks/useSongInfo';
import useSpotify from '../../hooks/useSpotify';
import { selectSongValue } from '../../slices/songSlice';

const SpotifyPlayerWeb = () => {
  const spotifyApi = useSpotify();

  const { songInfo } = useSelector(selectSongValue);

  useSongInfo();

  return (
    <div className=" bg-center-color-gray text-xs md:text-base">
      <SpotifyPlayer
        name="WebPlayer"
        showSaveIcon
        token={spotifyApi.getAccessToken()!}
        uris={[songInfo?.uri!, 'spotify:track:7nDIflSHQXzaa8zupxwv3U']}
        initialVolume={0.5}
        styles={{
          activeColor: '#00ff00',
          bgColor: '#000',
          color: '#fff',
          loaderColor: '#fff',
          sliderColor: '#0000ff',
          trackArtistColor: '#ccc',
          trackNameColor: '#FFF',
          sliderTrackColor: '#222',
        }}
      />
    </div>
  );
};

export default SpotifyPlayerWeb;
