import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { addPlaylistById } from '../../slices/playlistSlice';

interface AvatarPlaylistProps {
  image: string;
  title: string;
  description: string;
  id: string;
}

const AvatarPlaylist = ({
  image,
  title,
  description,
  id,
}: AvatarPlaylistProps) => {
  const dispatch = useDispatch();

  const addItemId = () => {
    dispatch(addPlaylistById(id));
  };

  return (
    <Link href={`/playlist/${id}`} className="cursor-pointer">
      <div
        className="flex items-center space-x-4 py-1.5 cursor-pointer"
        onClick={addItemId}
      >
        <img className="w-10 h-10 rounded-full" src={image} alt="" />
        <div className="font-medium dark:text-white">
          <div>{title}</div>
          <div className="text-sm text-gray-500 dark:text-gray-400">
            {description}
          </div>
        </div>
      </div>
    </Link>
  );
};

export default AvatarPlaylist;
