import Link from 'next/link';
import { useDispatch } from 'react-redux';
import useSpotify from '../../hooks/useSpotify';
import { addAlbumId } from '../../slices/albumSlice';
import { addArtistId } from '../../slices/artistSlice';
import { addPlaylistById } from '../../slices/playlistSlice';
import { addCurrentTrackId } from '../../slices/songSlice';

interface MiniCardProps {
  id: string;
  title: string;
  img: string;
  artists?: string;
  duration?: string;
  type: string;
  order: number;
  albumName?: string;
}

const MiniCard = ({
  title,
  img,
  id,
  artists,
  duration,
  type,
  order,
  albumName,
}: MiniCardProps) => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();

  const addItemId = () => {
    switch (type) {
      case 'track': {
        dispatch(addCurrentTrackId(id));
        break;
      }
      case 'playlist': {
        dispatch(addPlaylistById(id));
        break;
      }
      case 'artist': {
        dispatch(addArtistId(id));
      }
      case 'album': {
        dispatch(addAlbumId(id));
      }
      default: {
        break;
      }
    }
  };
  return (
    <Link href={`/${type}/${id}`}>
      <div
        className="grid grid-cols-2 text-gray-500 py-4 px-5 hover:bg-gray-900 rounderd-lg cursor-pointer"
        onClick={addItemId}
      >
        <div className="flex items-center space-x-4">
          <p>{order + 1}</p>
          <img className="h-10 w-10" src={img} alt={title} />

          <div>
            <p className="w-36 lg:w-64 text-white truncate">{title}</p>
            <p className="w-40">{artists}</p>
          </div>
        </div>

        <div className="flex items-center justify-between ml-auto md:ml-0">
          <p className="w-40 hidden md:inline">{albumName}</p>
          <p>{duration}</p>
        </div>
      </div>
    </Link>
  );
};

export default MiniCard;
