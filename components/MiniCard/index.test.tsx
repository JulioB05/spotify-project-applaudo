import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { store } from '../../store';
import mockSession from '../../testUtils/mockSession';
import MiniCard from '../MiniCard';

describe('MiniCard', () => {
  it('renders a MiniCard', () => {
    render(
      <SessionProvider session={mockSession}>
        <Provider store={store}>
          <MiniCard
            id={'test'}
            title={'test'}
            img={'test'}
            type={'album'}
            order={0}
          />
        </Provider>
      </SessionProvider>
    );
  });
});
