const mockProviders = {
  spotify: {
    id: 'spotify',
    name: 'Spotify',
    type: 'oauth',
    signinUrl: 'http://localhost:3000/api/auth/signin/spotify',
    callbackUrl: 'http://localhost:3000/api/auth/callback/spotify',
  },
};

export default mockProviders;
