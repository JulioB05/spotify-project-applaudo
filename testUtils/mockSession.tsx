const mockSession = {
  user: {
    name: 'juliobc05',
    email: 'juliobc05@outlook.com',
  },
  expires: '2022-11-08T21:32:56.042Z',
  accessToken:
    'BQC4RBNJOQuCA2EdVCI4QyaGN1I9OOyu9SbKB_UddkCEMR2ZfvwzbPSpZtCUGDKZhv5yiQhciQOHw-8MGyUfAppLDpb8bmP1PgJECRptaV1SCKnV84txfOryaag1zz2TkDYmgPXyqTp1fsgC1P3dt4DCoXs0O6mCEQ4PlxaGnCaO6Dl0mVTfGMdxG0kcYmufhiZ8tl-yK6r69YhVILIizA-Lwm_PD3x9EmzwOdnlH2R6Z7Jj1Uy1ip8w',
  refreshToken:
    'AQB0KsPd35i9e5MgLhVvkbvoLnqhaiHaApLwqC7mJXlKRWHd-4YS14ggJmSsulpjqw7vh2cGmJ6R8EBB4QdoFEKDTApqL9736UR6lBaXzB4Y9nr2F3W8pbdmzico3Bek5bM',
  username: 'juliobc05',
};

export default mockSession;
