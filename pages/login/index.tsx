import { ClientSafeProvider, getProviders, signIn } from 'next-auth/react';
import Head from 'next/head';

import { InferGetServerSidePropsType } from 'next/types';

type LoginProps = {
  providers: InferGetServerSidePropsType<typeof getServerSideProps>;
};

const Login = ({ providers }: LoginProps) => {
  return (
    <div className="flex flex-col items-center bg-black min-h-screen w-full justify-center ">
      <Head>
        <title>Spotify Login</title>
      </Head>
      <img
        className="w-52 mb-5"
        src="https://links.papareact.com/9xl"
        alt="Spotify-logo"
      />
      {providers &&
        (Object.values(providers) as unknown as ClientSafeProvider[]).map(
          (provider) => (
            <div key={provider.name}>
              <button
                className="bg-[#18D860] text-white p-5 rounded-full"
                onClick={() => signIn(provider.id, { callbackUrl: '/' })}
              >
                {`Login with
                ${provider.name}`}
              </button>
            </div>
          )
        )}
    </div>
  );
};

export default Login;

export async function getServerSideProps() {
  const providers = await getProviders();

  return {
    props: {
      providers,
    },
  };
}
