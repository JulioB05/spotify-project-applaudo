import { getSession, useSession } from 'next-auth/react';
import Head from 'next/head';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import CenterHome from '../components/CenterComponents/CenterHome';
import Sidebar from '../components/Sidebar';
import UserButton from '../components/UserButton';
import useSpotify from '../hooks/useSpotify';
import { addUserAccountLevel, addUserData } from '../slices/userSlice';

const Home = () => {
  const { data: session } = useSession();
  const dispatch = useDispatch();
  const spotifyApi = useSpotify();

  useEffect(() => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi.getMe().then(
          function (res) {
            dispatch(addUserAccountLevel(res.body.product));
            dispatch(addUserData(res.body));
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
      }
    })();
  }, [session]);

  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Homepage</title>
      </Head>
      <main className="flex text-white">
        <Sidebar />
        <UserButton />
        <CenterHome />
      </main>

      <div className="sticky bottom-0 text-white"></div>
    </div>
  );
};

export default Home;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
