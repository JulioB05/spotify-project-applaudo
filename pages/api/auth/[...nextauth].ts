

import NextAuth from 'next-auth';
import { JWT } from 'next-auth/jwt';
import SpotifyProvider from 'next-auth/providers/spotify';
import spotifyApi, { LOGIN_URL } from '../../../lib/spotify';

async function refreshAccessToken(token: JWT) {
	try {
		spotifyApi.setAccessToken(token.accessToken as string);
		spotifyApi.setRefreshToken(token.refreshToken as string);

		const { body: refreshedToken } = await spotifyApi.refreshAccessToken();
	

		return {
			...token,
			accessToken: refreshedToken.access_token,
			accessTokenExpires: Date.now() + refreshedToken.expires_in * 1000,
			refreshToken: refreshedToken.refresh_token ?? token.refreshToken,
		};
	} catch (error) {
		console.error(error);

		return {
			...token,
			error: 'RefreshAccessTokenError',
		};
	}
}

export default NextAuth({
	// Configure one or more authentication providers
	providers: [
		SpotifyProvider({
			clientId: process.env.NEXT_PUBLIC_CLIENT_ID!,
			clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRET!,
			
			authorization: LOGIN_URL,
		}),
		// ...add more providers here
	],
	secret: process.env.JWT_SECRET!,
	pages: {
		signIn: '/login',
	},
	callbacks: {
		async jwt({ token, account, user }) {
			// first scenario: initial sign in:
			if (account && user) {
				return {
					...token,
					accessToken: account.access_token,
					refreshToken: account.refresh_token,
					username: account.providerAccountId,
					accessTokenExpires: account.expires_at
						? account.expires_at * 1000 // we are handling expiry times in milliseconds hence * 1000.
						: undefined,
				};
			}

			// return previous token if the access token has not expired yet (second scenario: when we return to the page and the token is still valid):
			// TODO: BORRAR EL '!':
			if (Date.now() < (token.accessTokenExpires  as  Number)) {
	
				return token;
			}

			// access token has expired, so we need to refresh it (third scenario: when we return to the page and the token has expired):
		
			return await refreshAccessToken(token);
		},

		async session({ session, token }) {
			session.accessToken = token.accessToken;
			session.refreshToken = token.refreshToken;
			session.username = token.username;
			return session;
		},
	},
});
// export default NextAuth(authOptions)