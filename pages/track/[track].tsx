import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterSong from '../../components/CenterComponents/CenterSong';
import Sidebar from '../../components/Sidebar';
import useSongInfo from '../../hooks/useSongInfo';

const track = () => {
  useSongInfo();
  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Track</title>
      </Head>
      <main className="flex">
        <Sidebar />
        <CenterSong />
      </main>
    </div>
  );
};

export default track;
export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
