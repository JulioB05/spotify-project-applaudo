import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterLibrary from '../../components/CenterComponents/CenterLibrary';
import Sidebar from '../../components/Sidebar';

const Library = () => {
  return (
    <div className="bg-black h-screen overflow-hidden text-white">
      <Head>
        <title>Spotify Library</title>
      </Head>
      <main className="flex">
        <Sidebar />
        <CenterLibrary />
      </main>
    </div>
  );
};

export default Library;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
