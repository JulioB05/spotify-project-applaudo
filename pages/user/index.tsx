import { getSession } from 'next-auth/react';
import Head from 'next/head';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import MiniCard from '../../components/MiniCard';
import Sidebar from '../../components/Sidebar';
import Song from '../../components/Songs/song';
import userFavoriteData from '../../hooks/userFavoriteData';
import { selectUserValue } from '../../slices/userSlice';

const User = () => {
  const { userData } = useSelector(selectUserValue);

  const {
    getFavoriteArtists,
    getFavoriteTracks,
    getMyFollowedArtists,
    favoriteArtists,
    favoriteTracks,
    followedArtists,
  } = userFavoriteData();

  useEffect(() => {
    getFavoriteArtists();
    getFavoriteTracks();
    getMyFollowedArtists();
  }, []);

  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify User</title>
      </Head>
      <main className="flex">
        <Sidebar />

        <div className="flex-grow h-screen overflow-y-scroll scrollbar-hide  text-white">
          <section
            className={`flex items-center space-x-7 bg-center-color-blue text-white p-8 h-2/4`}
          >
            <img
              src={
                userData?.images?.[0]?.url
                  ? userData?.images?.[0]?.url
                  : 'https://thumbs.dreamstime.com/b/icono-de-usuario-predeterminado-vectores-imagen-perfil-avatar-predeterminada-vectorial-medios-sociales-retrato-182347582.jpg'
              }
              alt={userData?.id}
              className="h-60 w-60 shadow-2xl"
            />
            <div>
              <p>USER</p>
              <h1 className="text-2xl md:text-3xl xl:text-5xl font-bold pb-2">
                {userData?.display_name}
              </h1>

              <p className="pb-2 pt-2">{userData?.email}</p>
              <p className="pb-2 pt-2">
                Account: {userData?.product.toLocaleUpperCase()}
              </p>
              <p className="pb-2 pt-2">
                Followed: {followedArtists?.items?.length}
              </p>
            </div>
          </section>

          <section className="flex flex-col">
            {/* TOP TRACKS */}
            <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
              <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                TOP TRACKS
              </h1>
              {favoriteTracks?.length! > 0 ? (
                favoriteTracks?.map((track, i) => {
                  return (
                    <Song
                      key={track!.id + i + 1}
                      order={i}
                      track={track}
                      type={'playlist'}
                    />
                  );
                })
              ) : (
                <h1 className="flex items-center justify-center text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                  ---------- EMPTY ----------
                </h1>
              )}
            </div>
            {/* TOP ARTISTS */}
            <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
              <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                TOP ARTISTS
              </h1>
              {favoriteArtists?.length! > 0 ? (
                favoriteArtists?.map((artist, i) => {
                  const { id, name, images } = artist;
                  const img = images?.[0]?.url
                    ? images?.[0]?.url
                    : 'https://t2.tudocdn.net/600663?w=1920';
                  return (
                    <MiniCard
                      key={id}
                      id={id}
                      title={name}
                      img={img}
                      type={'artist'}
                      order={i}
                    />
                  );
                })
              ) : (
                <h1 className="flex items-center justify-center text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                  ---------- EMPTY ----------
                </h1>
              )}
            </div>
            {/* FOLLOWED ARTISTS */}
            <div className="px-8 flex flex-col space-y-1 pb-28 text-white">
              <h1 className="text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                FOLLOWED ARTISTS
              </h1>
              {followedArtists?.items.length! > 0 ? (
                followedArtists?.items?.map((follow, i) => {
                  const { id, name, images } = follow;
                  const img = images?.[0]?.url
                    ? images?.[0]?.url
                    : 'https://thumbs.dreamstime.com/b/icono-de-usuario-predeterminado-vectores-imagen-perfil-avatar-predeterminada-vectorial-medios-sociales-retrato-182347582.jpg';
                  return (
                    <MiniCard
                      key={id}
                      id={id}
                      title={name}
                      img={img}
                      type={'artist'}
                      order={i}
                    />
                  );
                })
              ) : (
                <h1 className="flex items-center justify-center text-xl md:text-2xl xl:text-3xl font-bold pb-2">
                  ---------- EMPTY ----------
                </h1>
              )}
            </div>
          </section>
        </div>
      </main>
    </div>
  );
};

export default User;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
