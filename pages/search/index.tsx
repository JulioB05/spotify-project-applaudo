import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterSearch from '../../components/CenterComponents/CenterSearch';
import Sidebar from '../../components/Sidebar';

const Search = () => {
  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Search</title>
      </Head>

      <main className="flex">
        <Sidebar />

        <CenterSearch />
        <div className="text-white flex "></div>
      </main>
    </div>
  );
};

export default Search;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
