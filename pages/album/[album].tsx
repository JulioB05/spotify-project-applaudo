import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterPlaylist from '../../components/CenterComponents/CenterPlaylist';

import Sidebar from '../../components/Sidebar';

const Album = () => {
  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Album</title>
      </Head>
      <main className="flex">
        <Sidebar />
        <CenterPlaylist type={'album'} />
      </main>
    </div>
  );
};

export default Album;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
