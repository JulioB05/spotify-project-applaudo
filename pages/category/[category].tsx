import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterCategory from '../../components/CenterComponents/CenterCategory';
import Sidebar from '../../components/Sidebar';

const Category = () => {
  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Category</title>
      </Head>
      <main className="flex">
        <Sidebar />
        <CenterCategory />
      </main>
    </div>
  );
};

export default Category;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
