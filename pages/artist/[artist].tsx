import { getSession } from 'next-auth/react';
import Head from 'next/head';
import CenterArtist from '../../components/CenterComponents/CenterArtist';
import Sidebar from '../../components/Sidebar';
import useArtistInformation from '../../hooks/useArtistInformation';

const Playlist = () => {
  useArtistInformation();

  return (
    <div className="bg-black h-screen overflow-hidden">
      <Head>
        <title>Spotify Artist</title>
      </Head>
      <main className="flex">
        <Sidebar />
        <CenterArtist />
      </main>
    </div>
  );
};

export default Playlist;

export async function getServerSideProps(context: Object) {
  const session = await getSession(context);

  return {
    props: {
      session,
    },
  };
}
