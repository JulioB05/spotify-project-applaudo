import { useEffect, useState } from 'react';
import useSpotify from './useSpotify';

export interface userFavoriteDataTypes {
  artists?:
    | SpotifyApi.PagingObject<SpotifyApi.ArtistObjectFull>
    | SpotifyApi.ArtistObjectFull[]
    | String;
  followedArtists?:
    | SpotifyApi.CursorBasedPagingObject<SpotifyApi.ArtistObjectFull>
    | String;
  tracks?:
    | SpotifyApi.PagingObject<SpotifyApi.TrackObjectFull>
    | SpotifyApi.TrackObjectFull[]
    | String;
}

const userFavoriteData = () => {
  const spotifyApi = useSpotify();

  const [favoriteArtists, setFavoriteArtists] =
    useState<SpotifyApi.ArtistObjectFull[]>();

  const [favoriteTracks, setFavoriteTracks] =
    useState<SpotifyApi.TrackObjectFull[]>();

  const [followedArtists, setFollowedArtists] =
    useState<SpotifyApi.CursorBasedPagingObject<SpotifyApi.ArtistObjectFull>>();

  const getFavoriteArtists = () => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi.getMyTopArtists({ limit: 5 }).then(
          function (data) {
            setFavoriteArtists(data.body.items);
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
      }
    })();
  };

  const getFavoriteTracks = () => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi.getMyTopTracks({ limit: 5 }).then(
          function (data) {
            setFavoriteTracks(data.body.items);
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
      }
    })();
  };

  const getMyFollowedArtists = () => {
    (async () => {
      if (spotifyApi.getAccessToken()) {
        spotifyApi.getFollowedArtists({ limit: 5 }).then(
          function (data) {
            setFollowedArtists(data.body.artists);
          },
          function (err) {
            console.error('Something went wrong!', err);
          }
        );
      }
    })();
  };

  useEffect(() => {
    if (spotifyApi.getAccessToken()) {
      getFavoriteArtists();
      getFavoriteTracks();
      getMyFollowedArtists();
    }
  }, []);

  return {
    getFavoriteArtists,
    getFavoriteTracks,
    getMyFollowedArtists,
    favoriteArtists,
    favoriteTracks,
    followedArtists,
  };
};

export default userFavoriteData;
