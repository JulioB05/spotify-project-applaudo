import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  addArtistAlbums,
  addArtistInfo,
  addArtistRelatedArtists,
  addArtistTopTracks,
  selectArtistValue,
} from '../slices/artistSlice';
import useSpotify from './useSpotify';

const useArtistInformation = () => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();
  const { artistId } = useSelector(selectArtistValue);

  useEffect(() => {
    if (spotifyApi.getAccessToken()) {
      (async () => {
        if (artistId !== '') {
          spotifyApi.getArtist(artistId!).then(
            function (res) {
              dispatch(addArtistInfo(res.body));
            },
            function (err) {
              console.error(err);
            }
          );
        }
      })();
      (async () => {
        if (artistId !== '') {
          spotifyApi.getArtistAlbums(artistId!).then(
            function (res) {
              dispatch(addArtistAlbums(res.body));
            },
            function (err) {
              console.error(err);
            }
          );
        }
      })();
      (async () => {
        if (artistId !== '') {
          spotifyApi.getArtistTopTracks(artistId!, 'MX').then(
            function (res) {
              dispatch(addArtistTopTracks(res.body));
            },
            function (err) {
              console.error(err);
            }
          );
        }
      })();
      (async () => {
        if (artistId !== '') {
          spotifyApi.getArtistRelatedArtists(artistId!).then(
            function (res) {
              dispatch(addArtistRelatedArtists(res.body));
            },
            function (err) {
              console.error(err);
            }
          );
        }
      })();
    }
  }, [artistId, spotifyApi]);
};

export default useArtistInformation;
