import { useState } from 'react';
import useSpotify from './useSpotify';

export interface DataFoundedTypes {
  artists?: SpotifyApi.PagingObject<SpotifyApi.ArtistObjectFull>;
  playlits?: SpotifyApi.PagingObject<SpotifyApi.PlaylistObjectSimplified>;
  tracks?: SpotifyApi.PagingObject<SpotifyApi.TrackObjectFull>;
  sizeOfData?: number;
}

const useDataFounded = () => {
  const spotifyApi = useSpotify();
  const [dataFounded, setDataFounded] = useState<DataFoundedTypes>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const dataObject: DataFoundedTypes = {};

  const getDataArtists = (page: number, search: string) => {
    const numberLimit = 21;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;

    if (spotifyApi.getAccessToken() && search !== '') {
      setIsLoading(false);
      spotifyApi
        .searchArtists(search, {
          limit: numberLimit,
          offset: indexOfFirstGame,
        })
        .then(
          function (data) {
            dataObject.artists = data.body.artists;
            dataObject.sizeOfData = data.body.artists?.total;
            setDataFounded(dataObject);
            setIsLoading(true);
          },
          function (err) {
            console.error(err);
          }
        );
    }
  };

  const getDataPlaylists = (page: number, search: string) => {
    const numberLimit = 21;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;

    if (spotifyApi.getAccessToken() && search !== '') {
      setIsLoading(false);
      spotifyApi
        .searchPlaylists(search, {
          limit: numberLimit,
          offset: indexOfFirstGame,
        })
        .then(
          function (data) {
            dataObject.playlits = data.body.playlists;
            dataObject.sizeOfData = data.body.playlists?.total;
            setDataFounded(dataObject);
            setIsLoading(true);
          },
          function (err) {
            console.error(err);
          }
        );
    }
  };
  const getDataTracks = (page: number, search: string) => {
    const numberLimit = 21;
    const indexOfLastGame = page * numberLimit;
    const indexOfFirstGame = indexOfLastGame - numberLimit;
    (async () => {
      if (spotifyApi.getAccessToken() && search !== '') {
        setIsLoading(false);
        spotifyApi
          .searchTracks(search, {
            limit: numberLimit,
            offset: indexOfFirstGame,
          })
          .then(
            function (data) {
              dataObject.sizeOfData = data.body.tracks?.total;
              dataObject.tracks = data.body.tracks;
              setDataFounded(dataObject);
              setIsLoading(true);
            },
            function (err) {
              console.error(err);
            }
          );
      }
    })();
  };

  return {
    getDataArtists,
    getDataPlaylists,
    getDataTracks,
    dataFounded,
    isLoading,
  };
};
export default useDataFounded;
