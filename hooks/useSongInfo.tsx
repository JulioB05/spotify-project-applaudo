import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addSongInfo, selectSongValue } from '../slices/songSlice';
import useSpotify from './useSpotify';

const useSongInfo = () => {
  const spotifyApi = useSpotify();
  const dispatch = useDispatch();
  const { currentTrackIdState } = useSelector(selectSongValue);

  useEffect(() => {
    (async () => {
      if (spotifyApi.getAccessToken() && currentTrackIdState !== '') {
        const data = spotifyApi.getTrack(currentTrackIdState!).then(
          function (res) {
            dispatch(addSongInfo(res.body));
          },
          function (err) {
            console.error(err);
          }
        );
      }
    })();
  }, [currentTrackIdState, spotifyApi]);
};

export default useSongInfo;

// (async () => {
// })();
