import { getToken } from 'next-auth/jwt';
import { NextRequest, NextResponse } from 'next/server';

export const config = {
  matcher: [
    '/',
    '/album/:id*',
    '/artist/:id*',
    '/library',
    '/playlist/:id*',
    '/search',
    '/track/:id*',
    '/user',
    '/category/:id*',
  ],
};

export async function middleware(req: NextRequest) {
  //Token will exist if user is logged in
  const token = await getToken({ req: req, secret: process.env.JWT_SECRET });

  const { pathname } = req.nextUrl;

  //Allow the request if the following is true...
  // 1) Its a request for next-auth session & provider fetching
  // 2) if the token exist

  if (pathname.includes('/api/auth') || token) {
    return NextResponse.next();
  }

  // Redirect them to login if they dont have token AND are requesting a protected route

  if (!token && pathname !== '/login') {
    return NextResponse.redirect(`${process.env.NEXTAUTH_URL}/login`);
  }
}
