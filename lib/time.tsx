import { toInteger } from 'lodash';

const timeConvertion = (millis: number) => {
  const minutes = Math.floor(millis / 60000);
  const seconds = toInteger(((millis % 60000) / 1000).toFixed(0));

  return seconds == 60
    ? minutes + 1 + ':00'
    : minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
};

export default timeConvertion;
